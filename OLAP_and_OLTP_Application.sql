-- Analyzing data for product categories
SELECT  p.prod_category,
        SUM(s.amount_sold) AS total_sales
FROM sh.sales s
JOIN 
sh.products p ON s.prod_id = p.prod_id
JOIN 
sh.times t ON s.time_id = t.time_id
WHERE 
t.time_id BETWEEN '1998-01-01' AND '1998-01-05'
GROUP BY  p.prod_category
ORDER BY total_sales DESC;

-- Analyzing average quantity sold for a specific product by country region
SELECT co.country_region,
       AVG(s.quantity_sold) AS avg_quantity_sold
FROM sh.sales s
JOIN 
sh.customers c ON s.cust_id = c.cust_id
JOIN 
sh.countries co ON c.country_id = co.country_id
WHERE s.prod_id = 8
GROUP BY co.country_region
ORDER BY avg_quantity_sold DESC;


-- Finding top 5 customers by total sales
SELECT 
    c.cust_id,
    c.cust_first_name,
    c.cust_last_name,
    SUM(s.amount_sold) AS total_sales
FROM sh.sales s
JOIN 
 sh.customers c ON s.cust_id = c.cust_id
GROUP BY c.cust_id, c.cust_first_name, c.cust_last_name
ORDER BY total_sales DESC
LIMIT 5;
